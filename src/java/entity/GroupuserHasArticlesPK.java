/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author root
 */
@Embeddable
public class GroupuserHasArticlesPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "groupuser_name")
    private String groupuserName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "articles_id")
    private int articlesId;

    public GroupuserHasArticlesPK() {
    }

    public GroupuserHasArticlesPK(String groupuserName, int articlesId) {
        this.groupuserName = groupuserName;
        this.articlesId = articlesId;
    }

    public String getGroupuserName() {
        return groupuserName;
    }

    public void setGroupuserName(String groupuserName) {
        this.groupuserName = groupuserName;
    }

    public int getArticlesId() {
        return articlesId;
    }

    public void setArticlesId(int articlesId) {
        this.articlesId = articlesId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (groupuserName != null ? groupuserName.hashCode() : 0);
        hash += (int) articlesId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GroupuserHasArticlesPK)) {
            return false;
        }
        GroupuserHasArticlesPK other = (GroupuserHasArticlesPK) object;
        if ((this.groupuserName == null && other.groupuserName != null) || (this.groupuserName != null && !this.groupuserName.equals(other.groupuserName))) {
            return false;
        }
        if (this.articlesId != other.articlesId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.GroupuserHasArticlesPK[ groupuserName=" + groupuserName + ", articlesId=" + articlesId + " ]";
    }
    
}
