/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.GroupuserHasArticles;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author root
 */
@Stateless
public class GroupuserHasArticlesFacade extends AbstractFacade<GroupuserHasArticles> {

    @PersistenceContext(unitName = "testBlogPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public GroupuserHasArticlesFacade() {
        super(GroupuserHasArticles.class);
    }
    
}
