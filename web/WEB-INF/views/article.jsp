<%-- 
    Document   : article
    Created on : 09.12.2021, 15:03:23
    Author     : limar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
 <div id="main">
            <aside class="leftAside">
                <h2>Отзывы</h2>
                <ul>
                    <li><a href="#">Властелин Колец - 10/10</a></li>
                    <li><a href="#">Гарри Поттер - 9/10</a></li>
                    <li><a href="#">Ведьмак - 8/10</a></li>
                    <li><a href="#">Незнайка - 7/10</a></li>
                    
                </ul>
            </aside>
                ${param.name}
            <section>
               <!-- <article>
                    <h1>Рецензия на книгу "Властелин Колец"</h1>
                    <div class="text-article">
                        Я каждый раз удивляюсь, как автору удалось так понятно изложить огромную
                        кучу событий, выстроить сложнейшую вселенную с кучей героев, ходов и выходов.
                        Это просто какая-то невероятная магия. Подробности сюжета и мира раскрываются
                        постепенно, наверное, очень здорово было бы читать эту книгу, не зная ничего
                        вообще ни о кольцах, ни о героях этой книги. И да, герои... их тут много,
                        но опять же, они появляются не все сразу. У каждого есть своя роль в истории,
                        для каждого история заканчивается, автор никого не забыл в конце, ничего здесь
                        не было случайным или ненужным.
                    </div>
                    <div class="fotter-article">
                        <span class="autor">Автор рецензии: <a href="#">Лимарь И.А.</a></span>
                        <span class="date-article">Дата написания: 10.12.2021</span>
                    </div>
                </article>-->
                <article>
                    <h1>${article.title}</h1>
                    <div class="text-article">
                        ${article.text}
                    </div>
                    <div class="fotter-article">
                        <span class="date-article"> Дата статьи: ${article.date}</span>
                    </div>
                </article>
               
            </section>
        </div>