<%-- 
    Document   : index
    Created on : 09.12.2021, 15:03:23
    Author     : limar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
        <div id="main">
            <aside class="leftAside">
                <h2>Бестселлеры этой недели</h2>
                <ul>
                    <li><a href="#">Гарри Поттер</a></li>
                    <li><a href="#">Ведьмак</a></li>
                    <li><a href="#">Властелин колец</a></li>
                    <li><a href="#">Незнайка на Луне</a></li>
                    
                </ul>
            </aside>
            <section>
                <!-- <article>
                    <h1>Гарри Поттер - Джоан Роулинг</h1>
                    <div class="text-article">
                        Популярная серия романов английской писательницы Джоан К. Роулинг. Каждый из романов описывает один год из жизни главного героя — мальчика - волшебника по имени Гарри Поттер. Все семь частей стали бестселлерами и переведены на множество языков, в том числе на русский.
                    </div>
                    <div class="fotter-article">
                        
                        <span class="read"><a href="javascript:void(0);">Читать...</a></span>
                        <span class="date-article">Дата публикации: 04.12.2021</span>
                    </div>
                </article>
                <article>
                    <h1>Властелин колец - Джон Толкин</h1>
                    <div class="text-article">
                        Роман-эпопея английского писателя Дж. Р. Р. Толкина, одно из самых известных произведений жанра фэнтези. «Властелин колец» был написан как единая книга, но из-за объёма при первом издании его разделили на три части — «Братство Кольца», «Две крепости» и «Возвращение короля».
                    </div>
                    <div class="fotter-article">
                        
                        <span class="read"><a href="javascript:void(0);">Читать...</a></span>
                        <span class="date-article">Дата публикации: 04.12.2021</span>
                        
                    </div>
                </article> -->
                
                <c:forEach var="article" items="${articles}">
                        <article>
                        <h1>${article.title}</h1>
                        <div class="text-article">
                        ${fn:substring(article.text,0,300)} ...
                        </div>
                        <div class="fotter-article">
                        <span class="read"><a href="article?id=${article.id}">

                                     Читать...</a></span>
                        <span class="date-article">Дата статьи: ${article.date}</span>
                        </div>
                        </article>
            </c:forEach>
                
            </section>
        </div>
       