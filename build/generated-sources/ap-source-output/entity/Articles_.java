package entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Articles.class)
public abstract class Articles_ {

	public static volatile SingularAttribute<Articles, Date> date;
	public static volatile SingularAttribute<Articles, Integer> id;
	public static volatile SingularAttribute<Articles, String> text;
	public static volatile SingularAttribute<Articles, String> title;

}

