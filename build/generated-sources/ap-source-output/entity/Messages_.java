package entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Messages.class)
public abstract class Messages_ {

	public static volatile SingularAttribute<Messages, Date> date;
	public static volatile SingularAttribute<Messages, String> usersLogin;
	public static volatile SingularAttribute<Messages, Integer> id;
	public static volatile SingularAttribute<Messages, String> text;
	public static volatile SingularAttribute<Messages, Integer> articlesId;

}

