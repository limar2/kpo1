package entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(GroupuserHasArticlesPK.class)
public abstract class GroupuserHasArticlesPK_ {

	public static volatile SingularAttribute<GroupuserHasArticlesPK, String> groupuserName;
	public static volatile SingularAttribute<GroupuserHasArticlesPK, Integer> articlesId;

}

